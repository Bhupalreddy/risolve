package com.github.paolorotolo.appintro;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.paolorotolo.appintro.util.CustomFontCache;
import com.github.paolorotolo.appintro.util.LogHelper;

public abstract class AppIntro extends AppIntroBase {
    private static final String TAG = LogHelper.makeLogTag(AppIntro.class);

    @Override
    protected int getLayoutId() {
        return R.layout.intro_layout;
    }

    /**
     * Override viewpager bar color
     *
     * @param color your color resource
     */
    public void setBarColor(@ColorInt final int color) {
        LinearLayout bottomBar = (LinearLayout) findViewById(R.id.bottom);
        bottomBar.setBackgroundColor(color);
    }

    /**
     * Override next button arrow color
     *
     * @param color your color
     */
    public void setNextArrowColor(@ColorInt final int color) {
        ImageButton nextButton = (ImageButton) findViewById(R.id.next);
        nextButton.setColorFilter(color);
    }

    /**
     * Override separator color
     *
     * @param color your color resource
     */
    public void setSeparatorColor(@ColorInt final int color) {
        TextView separator = (TextView) findViewById(R.id.bottom_separator);
        separator.setBackgroundColor(color);
    }

    /**
     * Override skip text typeface
     *
     * @param typeURL URL of font file located in Assets folder
     */
    public void setTextTypeface(@Nullable final String typeURL) {
        TextView signUpText = (TextView) findViewById(R.id.signUp);
        TextView loginText = (TextView) findViewById(R.id.login);
        if (CustomFontCache.get(typeURL, this) != null) {
            Typeface tf = CustomFontCache.get(typeURL, this);
            signUpText.setTypeface(tf);
            loginText.setTypeface(tf);
        }
    }

    /**
     * Override done button text color
     *
     * @param colorDoneText your color resource
     */
    public void setSignUpColorText(@ColorInt final int colorDoneText) {
        TextView signUpText = (TextView) findViewById(R.id.signUp);
        signUpText.setTextColor(colorDoneText);
    }

    public void setLoginColorText(@ColorInt final int colorDoneText) {
        TextView loginText = (TextView) findViewById(R.id.login);
        loginText.setTextColor(colorDoneText);
    }
    /**
     * Override Next button
     *
     * @param imageNextButton your drawable resource
     */
    public void setImageNextButton(final Drawable imageNextButton) {
        final ImageView nextButton = (ImageView) findViewById(R.id.next);
        nextButton.setImageDrawable(imageNextButton);
    }

    /**
     * Shows or hides Done button, replaced with setProgressButtonEnabled
     *
     * @deprecated use {@link #setProgressButtonEnabled(boolean)} instead.
     */
    @Deprecated
    public void showDoneButton(boolean showDone) {
        setProgressButtonEnabled(showDone);
    }

    /**
     * Show or hide the Separator line.
     * This is a static setting and Separator state is maintained across slides
     * until explicitly changed.
     *
     * @param showSeparator Set : true to display. false to hide.
     */
    public void showSeparator(boolean showSeparator) {
        TextView bottomSeparator = (TextView) findViewById(R.id.bottom_separator);
        if(showSeparator) {
            bottomSeparator.setVisibility(View.VISIBLE);
        } else {
            bottomSeparator.setVisibility(View.INVISIBLE);
        }
    }
}
