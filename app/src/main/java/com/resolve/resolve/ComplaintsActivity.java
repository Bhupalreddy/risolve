package com.resolve.resolve;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.resolve.resolve.Utils.BottomNavigationViewHelper;
import com.resolve.resolve.fragments.ComplaintFragment;
import com.resolve.resolve.fragments.HelpFragment;
import com.resolve.resolve.fragments.ProfileFragment;

import io.reactivex.disposables.CompositeDisposable;

public class ComplaintsActivity extends AppCompatActivity {

    public CompositeDisposable disposables = new CompositeDisposable();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_complaints:
                    selectedFragment = ComplaintFragment.newInstance();
                    getSupportActionBar().setTitle("Complaints");
                    break;
                case R.id.navigation_profile:
                    selectedFragment = ProfileFragment.newInstance();
                    getSupportActionBar().setTitle("Profile");
                    break;
                case R.id.navigation_help:
                    selectedFragment = HelpFragment.newInstance();
                    getSupportActionBar().setTitle("Help");
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        // http://www.truiton.com/2017/01/android-bottom-navigation-bar-example/
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        int pos = getIntent().getIntExtra("PAGE", 0);

        //Manually displaying the first fragment - one time only
        switch (pos) {
            case 0:
                navigation.setSelectedItemId(R.id.navigation_complaints);
                break;
            case 1:
                navigation.setSelectedItemId(R.id.navigation_profile);
                break;
            case 2:
                navigation.setSelectedItemId(R.id.navigation_help);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentById(R.id.content);
        if (f instanceof ComplaintFragment || f instanceof ProfileFragment || f instanceof HelpFragment)
            finish();
        else if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposables != null && disposables.size() > 0)
            disposables.clear();
    }
}
