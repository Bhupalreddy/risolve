package com.resolve.resolve;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.resolve.resolve.Utils.SampleSlide;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_app_intro);

        addSlide(SampleSlide.newInstance(R.layout.intro_custom_layout1));
        addSlide(SampleSlide.newInstance(R.layout.intro_custom_layout2));
        addSlide(SampleSlide.newInstance(R.layout.intro_custom_layout3));
        setIndicatorColor(Color.parseColor("#dc4439"), Color.parseColor("#d7d7d7"));
        setSignUpColorText(Color.parseColor("#FFFFFF"));
        setLoginColorText(Color.parseColor("#dc4439"));
        // SHOW or HIDE the statusbar
        showStatusBar(false);

        // Edit the color of the nav bar on Lollipop+ devices
        setNavBarColor("#3F51B5");

        // Hide Skip/Done button
        showSkipButton(false);
        showDoneButton(false);
    }

    @Override
    public void onLoginPressed() {
        super.onLoginPressed();
        startActivity(new Intent(AppIntroActivity.this, AuthenticateActivity.class));
        finish();
    }

    @Override
    public void onSignUpPressed() {
        super.onSignUpPressed();
        startActivity(new Intent(AppIntroActivity.this, AuthenticateActivity.class));
        finish();
    }
}
