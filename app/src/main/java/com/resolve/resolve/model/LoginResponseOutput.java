
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponseOutput {

    @Override
    public String toString() {
        return "LoginResponseOutput{" +
                "bmaUserId='" + bmaUserId + '\'' +
                ", bmaUserName='" + bmaUserName + '\'' +
                ", bmaUserEmail='" + bmaUserEmail + '\'' +
                ", bmaUserPassword='" + bmaUserPassword + '\'' +
                ", bmaUserMobile='" + bmaUserMobile + '\'' +
                ", bmaUserImage='" + bmaUserImage + '\'' +
                ", bmaUserTypeId='" + bmaUserTypeId + '\'' +
                ", bmaUserDepartmentId=" + bmaUserDepartmentId +
                ", bmaUserProjectId='" + bmaUserProjectId + '\'' +
                ", bmaUserBuildingId='" + bmaUserBuildingId + '\'' +
                ", bmaUserFlatId='" + bmaUserFlatId + '\'' +
                ", bmaUserCreatedDate='" + bmaUserCreatedDate + '\'' +
                ", bmaUserUpdatedDate='" + bmaUserUpdatedDate + '\'' +
                ", bmaUserContractorId=" + bmaUserContractorId +
                ", bmaUserSignature=" + bmaUserSignature +
                ", bmaUserStatus='" + bmaUserStatus + '\'' +
                ", mailed='" + mailed + '\'' +
                ", bmaProjectId='" + bmaProjectId + '\'' +
                ", bmaProjectName='" + bmaProjectName + '\'' +
                ", bmaProjectAddress='" + bmaProjectAddress + '\'' +
                ", bmaProjectShortCode='" + bmaProjectShortCode + '\'' +
                ", bmaProjectLongitude='" + bmaProjectLongitude + '\'' +
                ", bmaProjectLatitude='" + bmaProjectLatitude + '\'' +
                ", bmaProjectImage='" + bmaProjectImage + '\'' +
                ", bmaProjectCreatedDate='" + bmaProjectCreatedDate + '\'' +
                ", bmaProjectUpdatedDate='" + bmaProjectUpdatedDate + '\'' +
                ", bmaProjectStatus='" + bmaProjectStatus + '\'' +
                ", bmaBuildingId='" + bmaBuildingId + '\'' +
                ", bmaBuildingProjectId='" + bmaBuildingProjectId + '\'' +
                ", bmaBuildingName='" + bmaBuildingName + '\'' +
                ", bmaBuildingAddedDate='" + bmaBuildingAddedDate + '\'' +
                ", bmaBuildingUpdatedDate='" + bmaBuildingUpdatedDate + '\'' +
                ", bmaBuildingAddress='" + bmaBuildingAddress + '\'' +
                ", bmaBuildingLatitude='" + bmaBuildingLatitude + '\'' +
                ", bmaBuildingLongitude='" + bmaBuildingLongitude + '\'' +
                ", bmaBuildingStatus='" + bmaBuildingStatus + '\'' +
                ", bmaFlatId='" + bmaFlatId + '\'' +
                ", bmaFlatProjectId='" + bmaFlatProjectId + '\'' +
                ", bmaFlatBuildingId='" + bmaFlatBuildingId + '\'' +
                ", bmaFlatName='" + bmaFlatName + '\'' +
                ", bmaFlatAddedDate='" + bmaFlatAddedDate + '\'' +
                ", bmaFlatUpdatedDate='" + bmaFlatUpdatedDate + '\'' +
                ", bmaFlatLongitude='" + bmaFlatLongitude + '\'' +
                ", bmaFlatLatitude='" + bmaFlatLatitude + '\'' +
                ", bmaFlatStatus='" + bmaFlatStatus + '\'' +
                ", bmaUsertypeId='" + bmaUsertypeId + '\'' +
                ", bmaUsertypeName='" + bmaUsertypeName + '\'' +
                ", bmaUsertypeCreatedDate='" + bmaUsertypeCreatedDate + '\'' +
                ", bmaUsertypeUpdatedDate='" + bmaUsertypeUpdatedDate + '\'' +
                ", bmaUsertypeStatus='" + bmaUsertypeStatus + '\'' +
                '}';
    }

    @SerializedName("bma_user_id")
    @Expose
    private String bmaUserId;
    @SerializedName("bma_user_name")
    @Expose
    private String bmaUserName;
    @SerializedName("bma_user_email")
    @Expose
    private String bmaUserEmail;
    @SerializedName("bma_user_password")
    @Expose
    private String bmaUserPassword;
    @SerializedName("bma_user_mobile")
    @Expose
    private String bmaUserMobile;
    @SerializedName("bma_user_image")
    @Expose
    private String bmaUserImage;
    @SerializedName("bma_user_type_id")
    @Expose
    private String bmaUserTypeId;
    @SerializedName("bma_user_department_id")
    @Expose
    private Object bmaUserDepartmentId;
    @SerializedName("bma_user_project_id")
    @Expose
    private String bmaUserProjectId;
    @SerializedName("bma_user_building_id")
    @Expose
    private String bmaUserBuildingId;
    @SerializedName("bma_user_flat_id")
    @Expose
    private String bmaUserFlatId;
    @SerializedName("bma_user_created_date")
    @Expose
    private String bmaUserCreatedDate;
    @SerializedName("bma_user_updated_date")
    @Expose
    private String bmaUserUpdatedDate;
    @SerializedName("bma_user_contractor_id")
    @Expose
    private Object bmaUserContractorId;
    @SerializedName("bma_user_signature")
    @Expose
    private Object bmaUserSignature;
    @SerializedName("bma_user_status")
    @Expose
    private String bmaUserStatus;
    @SerializedName("mailed")
    @Expose
    private String mailed;
    @SerializedName("bma_project_id")
    @Expose
    private String bmaProjectId;
    @SerializedName("bma_project_name")
    @Expose
    private String bmaProjectName;
    @SerializedName("bma_project_address")
    @Expose
    private String bmaProjectAddress;
    @SerializedName("bma_project_short_code")
    @Expose
    private String bmaProjectShortCode;
    @SerializedName("bma_project_longitude")
    @Expose
    private String bmaProjectLongitude;
    @SerializedName("bma_project_latitude")
    @Expose
    private String bmaProjectLatitude;
    @SerializedName("bma_project_image")
    @Expose
    private String bmaProjectImage;
    @SerializedName("bma_project_created_date")
    @Expose
    private String bmaProjectCreatedDate;
    @SerializedName("bma_project_updated_date")
    @Expose
    private String bmaProjectUpdatedDate;
    @SerializedName("bma_project_status")
    @Expose
    private String bmaProjectStatus;
    @SerializedName("bma_building_id")
    @Expose
    private String bmaBuildingId;
    @SerializedName("bma_building_project_id")
    @Expose
    private String bmaBuildingProjectId;
    @SerializedName("bma_building_name")
    @Expose
    private String bmaBuildingName;
    @SerializedName("bma_building_added_date")
    @Expose
    private String bmaBuildingAddedDate;
    @SerializedName("bma_building_updated_date")
    @Expose
    private String bmaBuildingUpdatedDate;
    @SerializedName("bma_building_address")
    @Expose
    private String bmaBuildingAddress;
    @SerializedName("bma_building_latitude")
    @Expose
    private String bmaBuildingLatitude;
    @SerializedName("bma_building_longitude")
    @Expose
    private String bmaBuildingLongitude;
    @SerializedName("bma_building_status")
    @Expose
    private String bmaBuildingStatus;
    @SerializedName("bma_flat_id")
    @Expose
    private String bmaFlatId;
    @SerializedName("bma_flat_project_id")
    @Expose
    private String bmaFlatProjectId;
    @SerializedName("bma_flat_building_id")
    @Expose
    private String bmaFlatBuildingId;
    @SerializedName("bma_flat_name")
    @Expose
    private String bmaFlatName;
    @SerializedName("bma_flat_added_date")
    @Expose
    private String bmaFlatAddedDate;
    @SerializedName("bma_flat_updated_date")
    @Expose
    private String bmaFlatUpdatedDate;
    @SerializedName("bma_flat_longitude")
    @Expose
    private String bmaFlatLongitude;
    @SerializedName("bma_flat_latitude")
    @Expose
    private String bmaFlatLatitude;
    @SerializedName("bma_flat_status")
    @Expose
    private String bmaFlatStatus;
    @SerializedName("bma_usertype_id")
    @Expose
    private String bmaUsertypeId;
    @SerializedName("bma_usertype_name")
    @Expose
    private String bmaUsertypeName;
    @SerializedName("bma_usertype_created_date")
    @Expose
    private String bmaUsertypeCreatedDate;
    @SerializedName("bma_usertype_updated_date")
    @Expose
    private String bmaUsertypeUpdatedDate;
    @SerializedName("bma_usertype_status")
    @Expose
    private String bmaUsertypeStatus;

    public String getBmaUserId() {
        return bmaUserId;
    }

    public void setBmaUserId(String bmaUserId) {
        this.bmaUserId = bmaUserId;
    }

    public String getBmaUserName() {
        return bmaUserName;
    }

    public void setBmaUserName(String bmaUserName) {
        this.bmaUserName = bmaUserName;
    }

    public String getBmaUserEmail() {
        return bmaUserEmail;
    }

    public void setBmaUserEmail(String bmaUserEmail) {
        this.bmaUserEmail = bmaUserEmail;
    }

    public String getBmaUserPassword() {
        return bmaUserPassword;
    }

    public void setBmaUserPassword(String bmaUserPassword) {
        this.bmaUserPassword = bmaUserPassword;
    }

    public String getBmaUserMobile() {
        return bmaUserMobile;
    }

    public void setBmaUserMobile(String bmaUserMobile) {
        this.bmaUserMobile = bmaUserMobile;
    }

    public String getBmaUserImage() {
        return bmaUserImage;
    }

    public void setBmaUserImage(String bmaUserImage) {
        this.bmaUserImage = bmaUserImage;
    }

    public String getBmaUserTypeId() {
        return bmaUserTypeId;
    }

    public void setBmaUserTypeId(String bmaUserTypeId) {
        this.bmaUserTypeId = bmaUserTypeId;
    }

    public Object getBmaUserDepartmentId() {
        return bmaUserDepartmentId;
    }

    public void setBmaUserDepartmentId(Object bmaUserDepartmentId) {
        this.bmaUserDepartmentId = bmaUserDepartmentId;
    }

    public String getBmaUserProjectId() {
        return bmaUserProjectId;
    }

    public void setBmaUserProjectId(String bmaUserProjectId) {
        this.bmaUserProjectId = bmaUserProjectId;
    }

    public String getBmaUserBuildingId() {
        return bmaUserBuildingId;
    }

    public void setBmaUserBuildingId(String bmaUserBuildingId) {
        this.bmaUserBuildingId = bmaUserBuildingId;
    }

    public String getBmaUserFlatId() {
        return bmaUserFlatId;
    }

    public void setBmaUserFlatId(String bmaUserFlatId) {
        this.bmaUserFlatId = bmaUserFlatId;
    }

    public String getBmaUserCreatedDate() {
        return bmaUserCreatedDate;
    }

    public void setBmaUserCreatedDate(String bmaUserCreatedDate) {
        this.bmaUserCreatedDate = bmaUserCreatedDate;
    }

    public String getBmaUserUpdatedDate() {
        return bmaUserUpdatedDate;
    }

    public void setBmaUserUpdatedDate(String bmaUserUpdatedDate) {
        this.bmaUserUpdatedDate = bmaUserUpdatedDate;
    }

    public Object getBmaUserContractorId() {
        return bmaUserContractorId;
    }

    public void setBmaUserContractorId(Object bmaUserContractorId) {
        this.bmaUserContractorId = bmaUserContractorId;
    }

    public Object getBmaUserSignature() {
        return bmaUserSignature;
    }

    public void setBmaUserSignature(Object bmaUserSignature) {
        this.bmaUserSignature = bmaUserSignature;
    }

    public String getBmaUserStatus() {
        return bmaUserStatus;
    }

    public void setBmaUserStatus(String bmaUserStatus) {
        this.bmaUserStatus = bmaUserStatus;
    }

    public String getMailed() {
        return mailed;
    }

    public void setMailed(String mailed) {
        this.mailed = mailed;
    }

    public String getBmaProjectId() {
        return bmaProjectId;
    }

    public void setBmaProjectId(String bmaProjectId) {
        this.bmaProjectId = bmaProjectId;
    }

    public String getBmaProjectName() {
        return bmaProjectName;
    }

    public void setBmaProjectName(String bmaProjectName) {
        this.bmaProjectName = bmaProjectName;
    }

    public String getBmaProjectAddress() {
        return bmaProjectAddress;
    }

    public void setBmaProjectAddress(String bmaProjectAddress) {
        this.bmaProjectAddress = bmaProjectAddress;
    }

    public String getBmaProjectShortCode() {
        return bmaProjectShortCode;
    }

    public void setBmaProjectShortCode(String bmaProjectShortCode) {
        this.bmaProjectShortCode = bmaProjectShortCode;
    }

    public String getBmaProjectLongitude() {
        return bmaProjectLongitude;
    }

    public void setBmaProjectLongitude(String bmaProjectLongitude) {
        this.bmaProjectLongitude = bmaProjectLongitude;
    }

    public String getBmaProjectLatitude() {
        return bmaProjectLatitude;
    }

    public void setBmaProjectLatitude(String bmaProjectLatitude) {
        this.bmaProjectLatitude = bmaProjectLatitude;
    }

    public String getBmaProjectImage() {
        return bmaProjectImage;
    }

    public void setBmaProjectImage(String bmaProjectImage) {
        this.bmaProjectImage = bmaProjectImage;
    }

    public String getBmaProjectCreatedDate() {
        return bmaProjectCreatedDate;
    }

    public void setBmaProjectCreatedDate(String bmaProjectCreatedDate) {
        this.bmaProjectCreatedDate = bmaProjectCreatedDate;
    }

    public String getBmaProjectUpdatedDate() {
        return bmaProjectUpdatedDate;
    }

    public void setBmaProjectUpdatedDate(String bmaProjectUpdatedDate) {
        this.bmaProjectUpdatedDate = bmaProjectUpdatedDate;
    }

    public String getBmaProjectStatus() {
        return bmaProjectStatus;
    }

    public void setBmaProjectStatus(String bmaProjectStatus) {
        this.bmaProjectStatus = bmaProjectStatus;
    }

    public String getBmaBuildingId() {
        return bmaBuildingId;
    }

    public void setBmaBuildingId(String bmaBuildingId) {
        this.bmaBuildingId = bmaBuildingId;
    }

    public String getBmaBuildingProjectId() {
        return bmaBuildingProjectId;
    }

    public void setBmaBuildingProjectId(String bmaBuildingProjectId) {
        this.bmaBuildingProjectId = bmaBuildingProjectId;
    }

    public String getBmaBuildingName() {
        return bmaBuildingName;
    }

    public void setBmaBuildingName(String bmaBuildingName) {
        this.bmaBuildingName = bmaBuildingName;
    }

    public String getBmaBuildingAddedDate() {
        return bmaBuildingAddedDate;
    }

    public void setBmaBuildingAddedDate(String bmaBuildingAddedDate) {
        this.bmaBuildingAddedDate = bmaBuildingAddedDate;
    }

    public String getBmaBuildingUpdatedDate() {
        return bmaBuildingUpdatedDate;
    }

    public void setBmaBuildingUpdatedDate(String bmaBuildingUpdatedDate) {
        this.bmaBuildingUpdatedDate = bmaBuildingUpdatedDate;
    }

    public String getBmaBuildingAddress() {
        return bmaBuildingAddress;
    }

    public void setBmaBuildingAddress(String bmaBuildingAddress) {
        this.bmaBuildingAddress = bmaBuildingAddress;
    }

    public String getBmaBuildingLatitude() {
        return bmaBuildingLatitude;
    }

    public void setBmaBuildingLatitude(String bmaBuildingLatitude) {
        this.bmaBuildingLatitude = bmaBuildingLatitude;
    }

    public String getBmaBuildingLongitude() {
        return bmaBuildingLongitude;
    }

    public void setBmaBuildingLongitude(String bmaBuildingLongitude) {
        this.bmaBuildingLongitude = bmaBuildingLongitude;
    }

    public String getBmaBuildingStatus() {
        return bmaBuildingStatus;
    }

    public void setBmaBuildingStatus(String bmaBuildingStatus) {
        this.bmaBuildingStatus = bmaBuildingStatus;
    }

    public String getBmaFlatId() {
        return bmaFlatId;
    }

    public void setBmaFlatId(String bmaFlatId) {
        this.bmaFlatId = bmaFlatId;
    }

    public String getBmaFlatProjectId() {
        return bmaFlatProjectId;
    }

    public void setBmaFlatProjectId(String bmaFlatProjectId) {
        this.bmaFlatProjectId = bmaFlatProjectId;
    }

    public String getBmaFlatBuildingId() {
        return bmaFlatBuildingId;
    }

    public void setBmaFlatBuildingId(String bmaFlatBuildingId) {
        this.bmaFlatBuildingId = bmaFlatBuildingId;
    }

    public String getBmaFlatName() {
        return bmaFlatName;
    }

    public void setBmaFlatName(String bmaFlatName) {
        this.bmaFlatName = bmaFlatName;
    }

    public String getBmaFlatAddedDate() {
        return bmaFlatAddedDate;
    }

    public void setBmaFlatAddedDate(String bmaFlatAddedDate) {
        this.bmaFlatAddedDate = bmaFlatAddedDate;
    }

    public String getBmaFlatUpdatedDate() {
        return bmaFlatUpdatedDate;
    }

    public void setBmaFlatUpdatedDate(String bmaFlatUpdatedDate) {
        this.bmaFlatUpdatedDate = bmaFlatUpdatedDate;
    }

    public String getBmaFlatLongitude() {
        return bmaFlatLongitude;
    }

    public void setBmaFlatLongitude(String bmaFlatLongitude) {
        this.bmaFlatLongitude = bmaFlatLongitude;
    }

    public String getBmaFlatLatitude() {
        return bmaFlatLatitude;
    }

    public void setBmaFlatLatitude(String bmaFlatLatitude) {
        this.bmaFlatLatitude = bmaFlatLatitude;
    }

    public String getBmaFlatStatus() {
        return bmaFlatStatus;
    }

    public void setBmaFlatStatus(String bmaFlatStatus) {
        this.bmaFlatStatus = bmaFlatStatus;
    }

    public String getBmaUsertypeId() {
        return bmaUsertypeId;
    }

    public void setBmaUsertypeId(String bmaUsertypeId) {
        this.bmaUsertypeId = bmaUsertypeId;
    }

    public String getBmaUsertypeName() {
        return bmaUsertypeName;
    }

    public void setBmaUsertypeName(String bmaUsertypeName) {
        this.bmaUsertypeName = bmaUsertypeName;
    }

    public String getBmaUsertypeCreatedDate() {
        return bmaUsertypeCreatedDate;
    }

    public void setBmaUsertypeCreatedDate(String bmaUsertypeCreatedDate) {
        this.bmaUsertypeCreatedDate = bmaUsertypeCreatedDate;
    }

    public String getBmaUsertypeUpdatedDate() {
        return bmaUsertypeUpdatedDate;
    }

    public void setBmaUsertypeUpdatedDate(String bmaUsertypeUpdatedDate) {
        this.bmaUsertypeUpdatedDate = bmaUsertypeUpdatedDate;
    }

    public String getBmaUsertypeStatus() {
        return bmaUsertypeStatus;
    }

    public void setBmaUsertypeStatus(String bmaUsertypeStatus) {
        this.bmaUsertypeStatus = bmaUsertypeStatus;
    }

}
