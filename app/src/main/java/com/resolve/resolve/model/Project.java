
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Project {

    @SerializedName("bma_project_id")
    @Expose
    private String bmaProjectId;
    @SerializedName("bma_project_name")
    @Expose
    private String bmaProjectName;
    @SerializedName("bma_project_address")
    @Expose
    private String bmaProjectAddress;
    @SerializedName("bma_project_longitude")
    @Expose
    private String bmaProjectLongitude;
    @SerializedName("bma_project_latitude")
    @Expose
    private String bmaProjectLatitude;
    @SerializedName("bma_project_created_date")
    @Expose
    private String bmaProjectCreatedDate;
    @SerializedName("bma_project_updated_date")
    @Expose
    private String bmaProjectUpdatedDate;
    @SerializedName("bma_project_status")
    @Expose
    private String bmaProjectStatus;
    @SerializedName("i")
    @Expose
    private Integer i;

    public String getBmaProjectId() {
        return bmaProjectId;
    }

    public void setBmaProjectId(String bmaProjectId) {
        this.bmaProjectId = bmaProjectId;
    }

    public String getBmaProjectName() {
        return bmaProjectName;
    }

    public void setBmaProjectName(String bmaProjectName) {
        this.bmaProjectName = bmaProjectName;
    }

    public String getBmaProjectAddress() {
        return bmaProjectAddress;
    }

    public void setBmaProjectAddress(String bmaProjectAddress) {
        this.bmaProjectAddress = bmaProjectAddress;
    }

    public String getBmaProjectLongitude() {
        return bmaProjectLongitude;
    }

    public void setBmaProjectLongitude(String bmaProjectLongitude) {
        this.bmaProjectLongitude = bmaProjectLongitude;
    }

    public String getBmaProjectLatitude() {
        return bmaProjectLatitude;
    }

    public void setBmaProjectLatitude(String bmaProjectLatitude) {
        this.bmaProjectLatitude = bmaProjectLatitude;
    }

    public String getBmaProjectCreatedDate() {
        return bmaProjectCreatedDate;
    }

    public void setBmaProjectCreatedDate(String bmaProjectCreatedDate) {
        this.bmaProjectCreatedDate = bmaProjectCreatedDate;
    }

    public String getBmaProjectUpdatedDate() {
        return bmaProjectUpdatedDate;
    }

    public void setBmaProjectUpdatedDate(String bmaProjectUpdatedDate) {
        this.bmaProjectUpdatedDate = bmaProjectUpdatedDate;
    }

    public String getBmaProjectStatus() {
        return bmaProjectStatus;
    }

    public void setBmaProjectStatus(String bmaProjectStatus) {
        this.bmaProjectStatus = bmaProjectStatus;
    }

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

}
