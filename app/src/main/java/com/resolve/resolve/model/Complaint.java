
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Complaint {

    @SerializedName("complaint_id")
    @Expose
    private String complaintId;
    @SerializedName("complaint_number")
    @Expose
    private String complaintNumber;
    @SerializedName("complaint_user_id")
    @Expose
    private String complaintUserId;
    @SerializedName("complaint_subject")
    @Expose
    private String complaintSubject;
    @SerializedName("complaint_description")
    @Expose
    private String complaintDescription;
    @SerializedName("complaint_department_id")
    @Expose
    private String complaintDepartmentId;
    @SerializedName("complaint_images")
    @Expose
    private String complaintImages;
    @SerializedName("complaint_videos")
    @Expose
    private String complaintVideos;
    @SerializedName("complaint_availabulity_time1")
    @Expose
    private String complaintAvailabulityTime1;
    @SerializedName("complaint_availabulity_time2")
    @Expose
    private String complaintAvailabulityTime2;
    @SerializedName("complaint_acceptence_id")
    @Expose
    private Object complaintAcceptenceId;
    @SerializedName("complaint_user_feedback")
    @Expose
    private Object complaintUserFeedback;
    @SerializedName("complaint_technician_review")
    @Expose
    private Object complaintTechnicianReview;
    @SerializedName("complaint_technician_uplode_images")
    @Expose
    private Object complaintTechnicianUplodeImages;
    @SerializedName("complaint_user_signature")
    @Expose
    private Object complaintUserSignature;
    @SerializedName("complaint_estimated_id")
    @Expose
    private Object complaintEstimatedId;
    @SerializedName("complaint_assigned_id")
    @Expose
    private Object complaintAssignedId;
    @SerializedName("complaint_created_date")
    @Expose
    private String complaintCreatedDate;
    @SerializedName("complaint_updated_date")
    @Expose
    private String complaintUpdatedDate;
    @SerializedName("complaint_status")
    @Expose
    private String complaintStatus;
    @SerializedName("bma_user_name")
    @Expose
    private String bmaUserName;
    @SerializedName("bma_user_email")
    @Expose
    private String bmaUserEmail;
    @SerializedName("bma_user_mobile")
    @Expose
    private String bmaUserMobile;
    @SerializedName("bma_user_type_id")
    @Expose
    private String bmaUserTypeId;
    @SerializedName("bma_user_project_id")
    @Expose
    private String bmaUserProjectId;
    @SerializedName("bma_user_building_id")
    @Expose
    private String bmaUserBuildingId;
    @SerializedName("bma_user_flat_id")
    @Expose
    private String bmaUserFlatId;
    @SerializedName("bma_department_name")
    @Expose
    private String bmaDepartmentName;
    @SerializedName("bma_project_name")
    @Expose
    private String bmaProjectName;
    @SerializedName("bma_building_name")
    @Expose
    private String bmaBuildingName;
    @SerializedName("bma_flat_name")
    @Expose
    private String bmaFlatName;
    @SerializedName("bma_status_name")
    @Expose
    private String bmaStatusName;

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public String getComplaintUserId() {
        return complaintUserId;
    }

    public void setComplaintUserId(String complaintUserId) {
        this.complaintUserId = complaintUserId;
    }

    public String getComplaintSubject() {
        return complaintSubject;
    }

    public void setComplaintSubject(String complaintSubject) {
        this.complaintSubject = complaintSubject;
    }

    public String getComplaintDescription() {
        return complaintDescription;
    }

    public void setComplaintDescription(String complaintDescription) {
        this.complaintDescription = complaintDescription;
    }

    public String getComplaintDepartmentId() {
        return complaintDepartmentId;
    }

    public void setComplaintDepartmentId(String complaintDepartmentId) {
        this.complaintDepartmentId = complaintDepartmentId;
    }

    public String getComplaintImages() {
        return complaintImages;
    }

    public void setComplaintImages(String complaintImages) {
        this.complaintImages = complaintImages;
    }

    public String getComplaintVideos() {
        return complaintVideos;
    }

    public void setComplaintVideos(String complaintVideos) {
        this.complaintVideos = complaintVideos;
    }

    public String getComplaintAvailabulityTime1() {
        return complaintAvailabulityTime1;
    }

    public void setComplaintAvailabulityTime1(String complaintAvailabulityTime1) {
        this.complaintAvailabulityTime1 = complaintAvailabulityTime1;
    }

    public String getComplaintAvailabulityTime2() {
        return complaintAvailabulityTime2;
    }

    public void setComplaintAvailabulityTime2(String complaintAvailabulityTime2) {
        this.complaintAvailabulityTime2 = complaintAvailabulityTime2;
    }

    public Object getComplaintAcceptenceId() {
        return complaintAcceptenceId;
    }

    public void setComplaintAcceptenceId(Object complaintAcceptenceId) {
        this.complaintAcceptenceId = complaintAcceptenceId;
    }

    public Object getComplaintUserFeedback() {
        return complaintUserFeedback;
    }

    public void setComplaintUserFeedback(Object complaintUserFeedback) {
        this.complaintUserFeedback = complaintUserFeedback;
    }

    public Object getComplaintTechnicianReview() {
        return complaintTechnicianReview;
    }

    public void setComplaintTechnicianReview(Object complaintTechnicianReview) {
        this.complaintTechnicianReview = complaintTechnicianReview;
    }

    public Object getComplaintTechnicianUplodeImages() {
        return complaintTechnicianUplodeImages;
    }

    public void setComplaintTechnicianUplodeImages(Object complaintTechnicianUplodeImages) {
        this.complaintTechnicianUplodeImages = complaintTechnicianUplodeImages;
    }

    public Object getComplaintUserSignature() {
        return complaintUserSignature;
    }

    public void setComplaintUserSignature(Object complaintUserSignature) {
        this.complaintUserSignature = complaintUserSignature;
    }

    public Object getComplaintEstimatedId() {
        return complaintEstimatedId;
    }

    public void setComplaintEstimatedId(Object complaintEstimatedId) {
        this.complaintEstimatedId = complaintEstimatedId;
    }

    public Object getComplaintAssignedId() {
        return complaintAssignedId;
    }

    public void setComplaintAssignedId(Object complaintAssignedId) {
        this.complaintAssignedId = complaintAssignedId;
    }

    public String getComplaintCreatedDate() {
        return complaintCreatedDate;
    }

    public void setComplaintCreatedDate(String complaintCreatedDate) {
        this.complaintCreatedDate = complaintCreatedDate;
    }

    public String getComplaintUpdatedDate() {
        return complaintUpdatedDate;
    }

    public void setComplaintUpdatedDate(String complaintUpdatedDate) {
        this.complaintUpdatedDate = complaintUpdatedDate;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getBmaUserName() {
        return bmaUserName;
    }

    public void setBmaUserName(String bmaUserName) {
        this.bmaUserName = bmaUserName;
    }

    public String getBmaUserEmail() {
        return bmaUserEmail;
    }

    public void setBmaUserEmail(String bmaUserEmail) {
        this.bmaUserEmail = bmaUserEmail;
    }

    public String getBmaUserMobile() {
        return bmaUserMobile;
    }

    public void setBmaUserMobile(String bmaUserMobile) {
        this.bmaUserMobile = bmaUserMobile;
    }

    public String getBmaUserTypeId() {
        return bmaUserTypeId;
    }

    public void setBmaUserTypeId(String bmaUserTypeId) {
        this.bmaUserTypeId = bmaUserTypeId;
    }

    public String getBmaUserProjectId() {
        return bmaUserProjectId;
    }

    public void setBmaUserProjectId(String bmaUserProjectId) {
        this.bmaUserProjectId = bmaUserProjectId;
    }

    public String getBmaUserBuildingId() {
        return bmaUserBuildingId;
    }

    public void setBmaUserBuildingId(String bmaUserBuildingId) {
        this.bmaUserBuildingId = bmaUserBuildingId;
    }

    public String getBmaUserFlatId() {
        return bmaUserFlatId;
    }

    public void setBmaUserFlatId(String bmaUserFlatId) {
        this.bmaUserFlatId = bmaUserFlatId;
    }

    public String getBmaDepartmentName() {
        return bmaDepartmentName;
    }

    public void setBmaDepartmentName(String bmaDepartmentName) {
        this.bmaDepartmentName = bmaDepartmentName;
    }

    public String getBmaProjectName() {
        return bmaProjectName;
    }

    public void setBmaProjectName(String bmaProjectName) {
        this.bmaProjectName = bmaProjectName;
    }

    public String getBmaBuildingName() {
        return bmaBuildingName;
    }

    public void setBmaBuildingName(String bmaBuildingName) {
        this.bmaBuildingName = bmaBuildingName;
    }

    public String getBmaFlatName() {
        return bmaFlatName;
    }

    public void setBmaFlatName(String bmaFlatName) {
        this.bmaFlatName = bmaFlatName;
    }

    public String getBmaStatusName() {
        return bmaStatusName;
    }

    public void setBmaStatusName(String bmaStatusName) {
        this.bmaStatusName = bmaStatusName;
    }

}
