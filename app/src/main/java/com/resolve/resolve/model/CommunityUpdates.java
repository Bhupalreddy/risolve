
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommunityUpdates {

    @SerializedName("notices")
    @Expose
    private ArrayList<Notice> notices = null;
    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;

    public ArrayList<Notice> getNotices() {
        return notices;
    }

    public void setNotices(ArrayList<Notice> notices) {
        this.notices = notices;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
