
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad {

    @SerializedName("bma_ad_id")
    @Expose
    private String bmaAdId;
    @SerializedName("bma_ad_name")
    @Expose
    private String bmaAdName;
    @SerializedName("bma_ad_description")
    @Expose
    private String bmaAdDescription;
    @SerializedName("bma_ad_url")
    @Expose
    private String bmaAdUrl;
    @SerializedName("bma_ad_image")
    @Expose
    private String bmaAdImage;
    @SerializedName("bma_start_date")
    @Expose
    private String bmaStartDate;
    @SerializedName("bma_end_date")
    @Expose
    private String bmaEndDate;
    @SerializedName("bma_ad_time")
    @Expose
    private String bmaAdTime;
    @SerializedName("bma_ad_project_id")
    @Expose
    private String bmaAdProjectId;
    @SerializedName("bma_project_name")
    @Expose
    private String bmaProjectName;
    @SerializedName("bma_ad_created_date")
    @Expose
    private String bmaAdCreatedDate;
    @SerializedName("bma_ad_updated_date")
    @Expose
    private String bmaAdUpdatedDate;
    @SerializedName("bma_ad_status")
    @Expose
    private String bmaAdStatus;

    public String getBmaAdId() {
        return bmaAdId;
    }

    public void setBmaAdId(String bmaAdId) {
        this.bmaAdId = bmaAdId;
    }

    public String getBmaAdName() {
        return bmaAdName;
    }

    public void setBmaAdName(String bmaAdName) {
        this.bmaAdName = bmaAdName;
    }

    public String getBmaAdDescription() {
        return bmaAdDescription;
    }

    public void setBmaAdDescription(String bmaAdDescription) {
        this.bmaAdDescription = bmaAdDescription;
    }

    public String getBmaAdUrl() {
        return bmaAdUrl;
    }

    public void setBmaAdUrl(String bmaAdUrl) {
        this.bmaAdUrl = bmaAdUrl;
    }

    public String getBmaAdImage() {
        return bmaAdImage;
    }

    public void setBmaAdImage(String bmaAdImage) {
        this.bmaAdImage = bmaAdImage;
    }

    public String getBmaStartDate() {
        return bmaStartDate;
    }

    public void setBmaStartDate(String bmaStartDate) {
        this.bmaStartDate = bmaStartDate;
    }

    public String getBmaEndDate() {
        return bmaEndDate;
    }

    public void setBmaEndDate(String bmaEndDate) {
        this.bmaEndDate = bmaEndDate;
    }

    public String getBmaAdTime() {
        return bmaAdTime;
    }

    public void setBmaAdTime(String bmaAdTime) {
        this.bmaAdTime = bmaAdTime;
    }

    public String getBmaAdProjectId() {
        return bmaAdProjectId;
    }

    public void setBmaAdProjectId(String bmaAdProjectId) {
        this.bmaAdProjectId = bmaAdProjectId;
    }

    public String getBmaProjectName() {
        return bmaProjectName;
    }

    public void setBmaProjectName(String bmaProjectName) {
        this.bmaProjectName = bmaProjectName;
    }

    public String getBmaAdCreatedDate() {
        return bmaAdCreatedDate;
    }

    public void setBmaAdCreatedDate(String bmaAdCreatedDate) {
        this.bmaAdCreatedDate = bmaAdCreatedDate;
    }

    public String getBmaAdUpdatedDate() {
        return bmaAdUpdatedDate;
    }

    public void setBmaAdUpdatedDate(String bmaAdUpdatedDate) {
        this.bmaAdUpdatedDate = bmaAdUpdatedDate;
    }

    public String getBmaAdStatus() {
        return bmaAdStatus;
    }

    public void setBmaAdStatus(String bmaAdStatus) {
        this.bmaAdStatus = bmaAdStatus;
    }

}
