
package com.resolve.resolve.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("output")
    @Expose
    private LoginResponseOutput output;

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LoginResponseOutput getOutput() {
        return output;
    }

    public void setOutput(LoginResponseOutput loginResponseOutput) {
        this.output = loginResponseOutput;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "msgCode=" + msgCode +
                ", msg='" + msg + '\'' +
                ", output=" + output.toString() +
                '}';
    }
}
