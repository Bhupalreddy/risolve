package com.resolve.resolve.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.resolve.resolve.R;
import com.resolve.resolve.model.Notice;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by srikrishna on 08-07-2017.
 */

public class CommunityUpdatesAdapter extends RecyclerView.Adapter<CommunityUpdatesAdapter.ViewHolder> {

    private static final String TAG = "CommunityUpdatesAdapter";

    private ArrayList<Notice> mDataSet;
    private float dp;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDesc, tvWhen, tvWhere;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvDesc = (TextView) v.findViewById(R.id.tvDesc);
            tvWhen = (TextView) v.findViewById(R.id.tvWhen);
            tvWhere = (TextView) v.findViewById(R.id.tvWhere);
        }
    }

    public CommunityUpdatesAdapter(ArrayList<Notice> dataSet) {
        mDataSet = dataSet;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1/* 1 pixel */, displaymetrics );
    }

    public void refresh(ArrayList<Notice> dataSet){
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    @Override
    public CommunityUpdatesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.community_update_item, viewGroup, false);

        return new CommunityUpdatesAdapter.ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CommunityUpdatesAdapter.ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        Notice cDO = mDataSet.get(position);
        viewHolder.tvTitle.setText(cDO.getBmaNoticeName());
        viewHolder.tvDesc.setText(cDO.getBmaNoticeDescription());
        viewHolder.tvWhen.setText(cDO.getBmaNoticeNumDays());
        viewHolder.tvWhere.setText(cDO.getBmaProjectAddress());
        String imagePath = TextUtils.isEmpty(cDO.getBmaNoticeImage()) ? null : cDO.getBmaNoticeImage();
        Picasso.with(viewHolder.ivImage.getContext())
                .load(imagePath)
//                .resize((int)(110/dp), (int)(110/dp))
//                .centerCrop()
                .placeholder(R.drawable.empty_state)
                .error(R.drawable.banner)
                .into(viewHolder.ivImage);
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
