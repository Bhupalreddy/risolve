package com.resolve.resolve.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.resolve.resolve.R;

/**
 * Created by srikrishna on 08-07-2017.
 */

public class AddComplaintFragment extends Fragment {

    public static AddComplaintFragment newInstance() {
        AddComplaintFragment fragment = new AddComplaintFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // http://onra.github.io/android/2014/10/16/android-dotted-border-layout.html
        View v = inflater.inflate(R.layout.new_complaint_fragment, container, false);
        ActionBar aBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        View aView = inflater.inflate(R.layout.actionbar_new_complaint_title, null);
        aBar.setCustomView(aView);
        aBar.setDisplayHomeAsUpEnabled(true);


        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.department_array, R.layout.area_spinner_tv);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        return v;
    }

}
