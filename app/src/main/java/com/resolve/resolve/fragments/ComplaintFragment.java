package com.resolve.resolve.fragments;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.resolve.resolve.App;
import com.resolve.resolve.ComplaintsActivity;
import com.resolve.resolve.R;
import com.resolve.resolve.adapters.ComplaintsAdapter;
import com.resolve.resolve.model.Complaint;
import com.resolve.resolve.model.Complaints;
import com.resolve.resolve.web.WebAPI;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static io.reactivex.plugins.RxJavaPlugins.onError;

/**
 * Created by srikrishna on 08-07-2017.
 */

public class ComplaintFragment extends Fragment {

    protected RecyclerView rvComplaints;
    protected TextView tvNoComplaints;
    protected RecyclerView.LayoutManager lmComplanits;
    protected ComplaintsAdapter aComplanits;
    @Inject WebAPI mWebAPI;
    @Inject SharedPreferences sPref;

    public static ComplaintFragment newInstance() {
        ComplaintFragment fragment = new ComplaintFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.complaint_fragment, container, false);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);

        rvComplaints = (RecyclerView) v.findViewById(R.id.rvComplaints);
        tvNoComplaints = (TextView) v.findViewById(R.id.tvNoComplaints);
        lmComplanits = new LinearLayoutManager(getActivity());
        rvComplaints.setLayoutManager(lmComplanits);
        aComplanits = new ComplaintsAdapter(null);
        rvComplaints.setAdapter(aComplanits);

        initSwipe();

        FloatingActionButton myFab = (FloatingActionButton) v.findViewById(R.id.fab_add);
        myFab.setOnClickListener(v1 -> {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, AddComplaintFragment.newInstance());
            transaction.addToBackStack(null);
            transaction.commit();
        });

        Observable<Complaints> call = mWebAPI.getComplaints(sPref.getString("bmaUserId", ""));
        Disposable callComplaints = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(complaints -> processComplaints(complaints),
                        throwable -> onError(throwable));
        ((ComplaintsActivity)getActivity()).disposables.add(callComplaints);

        return v;
    }

    private void processComplaints(Complaints complaints){
        ArrayList<Complaint> complaintsList = complaints.getComplaints();
        boolean isNonEmpty = complaintsList != null && complaintsList.size() > 0;
        rvComplaints.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        tvNoComplaints.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        aComplanits.refresh(complaintsList);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.complaint_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // perform query here

                // workaround to avoid issues with some emulators and keyboard devices firing twice if a keyboard enter is used
                // see https://code.google.com/p/android/issues/detail?id=24599
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Complaints");
    }

    private Paint p = new Paint();

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    aComplanits.removeItem(position);
                } else {
//                    removeView();
//                    edit_position = position;
//                    alertDialog.setTitle("Edit Country");
//                    et_country.setText(countries.get(position));
//                    alertDialog.show();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#379ae0"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.view);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#c94242"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.shape);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvComplaints);
    }

}
