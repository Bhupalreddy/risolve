package com.resolve.resolve;

import android.app.Application;

import com.resolve.resolve.web.AppModule;
import com.resolve.resolve.web.DaggerNetComponent;
import com.resolve.resolve.web.NetComponent;
import com.resolve.resolve.web.NetModule;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by srikrishna on 02-07-2017.
 */

public class App extends Application {

    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        // https://github.com/chrisjenx/Calligraphy
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // Dagger%COMPONENT_NAME%
        mNetComponent = DaggerNetComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .netModule(new NetModule("http://kothwal.in/"))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

}
