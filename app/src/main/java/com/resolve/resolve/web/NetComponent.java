package com.resolve.resolve.web;

import com.resolve.resolve.AuthenticateActivity;
import com.resolve.resolve.DashboardActivity;
import com.resolve.resolve.fragments.ComplaintFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by srikrishna on 11-07-2017.
 */

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(AuthenticateActivity activity);
    void inject(DashboardActivity activity);
    void inject(ComplaintFragment fragment);
    // void inject(MyFragment fragment);
    // void inject(MyService service);
}
