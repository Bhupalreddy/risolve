package com.resolve.resolve.web;

import com.resolve.resolve.model.CommunityUpdates;
import com.resolve.resolve.model.Complaints;
import com.resolve.resolve.model.ExclusiveOffers;
import com.resolve.resolve.model.LoginResponse;
import com.resolve.resolve.model.Projects;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by srikrishna on 10-07-2017.
 */

public interface  WebAPI {

    @FormUrlEncoded
    @POST("/admin/webservices/login.php")
    Observable<LoginResponse> getLogin(@FieldMap Map<String, String> names);

    @POST("/admin/webservices/projects.php")
    Observable<Projects> getProjects();

    @POST("/admin/webservices/ads.php")
    Observable<ExclusiveOffers> getExclusiveOffers(@Query("project_id") String projectId);

    @POST("/admin/webservices/notice-board.php")
    Observable<CommunityUpdates> getCommunityUpdates(
            @Query("project_id") String projectId,  // 1
            @Query("limit") String limit,           // 10
            @Query("offset") String offset);        // 0

    @POST("/admin/webservices/post_complaint.php")
    Observable<Complaints> getComplaints(@Query("user_id") String userId);
}
