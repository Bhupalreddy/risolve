package com.resolve.resolve;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.resolve.resolve.adapters.CommunityUpdatesAdapter;
import com.resolve.resolve.adapters.ExclusiveOffersAdapter;
import com.resolve.resolve.model.Ad;
import com.resolve.resolve.model.CommunityUpdates;
import com.resolve.resolve.model.ExclusiveOffers;
import com.resolve.resolve.model.Notice;
import com.resolve.resolve.web.WebAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DashboardActivity extends AppCompatActivity {


    @Inject SharedPreferences sPref;
    @Inject WebAPI mWebAPI;

    @BindView(R.id.rvExOffers) RecyclerView rvExOffers;
    @BindView(R.id.rvCommunityUpdates) RecyclerView rvCommunityUpdates;
    @BindView(R.id.tvNoExOf) TextView tvNoExOf;
    @BindView(R.id.tvNoCoUp) TextView tvNoCoUp;
    @BindView(R.id.tvCompaints) TextView tvCompaints;
    @BindView(R.id.tvProfile) TextView tvProfile;
    @BindView(R.id.tvHelp) TextView tvHelp;
    @BindView(R.id.ivBanner) ImageView ivBanner;

    protected RecyclerView.LayoutManager lmExOffers, lmCommunityUpdates;
    protected ExclusiveOffersAdapter aExOffers;
    protected CommunityUpdatesAdapter aCommunityUpdates;

    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);

        String imgUrl = sPref.getString("bmaProjectImage", null);
        Picasso.with(this)
                .load(imgUrl)
                .placeholder(R.drawable.progress_animation)
                .into(ivBanner);

        lmExOffers = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvExOffers.setLayoutManager(lmExOffers);
        aExOffers = new ExclusiveOffersAdapter(null);
        rvExOffers.setAdapter(aExOffers);

        lmCommunityUpdates = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCommunityUpdates.setLayoutManager(lmCommunityUpdates);
        aCommunityUpdates = new CommunityUpdatesAdapter(null);
        rvCommunityUpdates.setAdapter(aCommunityUpdates);

        tvCompaints.setOnClickListener(v -> goToPage(0));
        tvProfile.setOnClickListener(v ->  goToPage(1));
        tvHelp.setOnClickListener(v -> goToPage(2));

        callWebForContent();
    }

    private void callWebForContent(){

        String projectId = sPref.getString("bmaProjectId", "");

        Observable<ExclusiveOffers> callEOs = mWebAPI.getExclusiveOffers(projectId);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processEOs(x), x -> onError(x));
        disposables.add(callLogin);

        Observable<CommunityUpdates> call = mWebAPI.getCommunityUpdates(projectId, "10", "0");
        Disposable callCUs = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processCUs(x), x -> onError(x));
        disposables.add(callCUs);
    }

    private void processEOs(ExclusiveOffers exclusiveOffers){
        ArrayList<Ad> ads = exclusiveOffers.getAds();
        boolean isNonEmpty = ads != null && ads.size() > 0;
        rvCommunityUpdates.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        tvNoCoUp.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        aExOffers.refresh(ads);
    }

    private void processCUs(CommunityUpdates communityUpdates){
        ArrayList<Notice> notices = communityUpdates.getNotices();
        boolean isNonEmpty = notices != null && notices.size() > 0;
        rvExOffers.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        tvNoExOf.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        aCommunityUpdates.refresh(notices);
    }

    private void onError(Throwable throwable) {
        Snackbar.make(tvCompaints, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void goToPage(int page){
        Intent i = new Intent(DashboardActivity.this, ComplaintsActivity.class);
        i.putExtra("PAGE", page);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(disposables != null && disposables.size() > 0)
            disposables.clear();
    }
}
